from django.db import models

# Create your models here.
class Siswa(models.Model):
    nisn = models.CharField(max_length=50)
    nama = models.CharField(max_length=50)
    jenis_kelamin = models.CharField(max_length=50)
    kelas = models.CharField(max_length=50)
    
    def __str__(self):
        return self.nama