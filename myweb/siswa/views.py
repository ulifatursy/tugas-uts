from tempfile import template
from django.shortcuts import render
from .models import *
# Create your views here.

def siswa(request):
	hasil = Siswa.objects.all()
	template = 'siswa.html'
	context = {
		'siswa_siswa' : hasil,
	}
	return render(request, template, context)
	