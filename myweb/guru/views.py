from tempfile import template
from django.shortcuts import render
from .models import *
# Create your views here.

def guru(request):
	tampil = Guru.objects.all()
	template = 'guru.html'
	context = {
		'guru_guru' : tampil,
	}
	return render(request, template, context)