from django.db import models

# Create your models here.
class Guru(models.Model):
    nama = models.CharField(max_length=50)
    nip = models.CharField(max_length=50)
    alamat = models.CharField(max_length=50)

    def __str__(self):
        return self.list