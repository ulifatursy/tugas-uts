# Generated by Django 4.1.3 on 2022-11-08 06:36

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Guru',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=50)),
                ('nip', models.CharField(max_length=50)),
                ('alamat', models.CharField(max_length=50)),
            ],
        ),
    ]
